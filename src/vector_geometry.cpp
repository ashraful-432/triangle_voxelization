#include <bits/stdc++.h>
using namespace std;
#include "vector_geometry.h"

double dotProduct(C_VECTOR a, C_VECTOR b)
{
    double c = (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
    return c;
}
void crossProduct(C_VECTOR *c, C_VECTOR a, C_VECTOR b)
{
    c->x = (a.y * b.z) - (a.z * b.y);
    c->y = (a.z * b.x) - (a.x * b.z);
    c->z = (a.x * b.y) - (a.y * b.x);
}
void vec_scalar_mul_add_2(C_VECTOR *SUM, double a, C_VECTOR A, double b, C_VECTOR B)
{
    SUM->x = (a * A.x) + (b * B.x);
    SUM->y = (a * A.y) + (b * B.y);
    SUM->z = (a * A.z) + (b * B.z);
}
void vec_scalar_mul_add_3(C_VECTOR *SUM, double a, C_VECTOR A, double b, C_VECTOR B, double c, C_VECTOR C)
{
    //scalar = +1 or -1 or other literals
    SUM->x = (a * (A.x)) + (b * (B.x)) + (c * (C.x));
    SUM->y = (a * (A.y)) + (b * (B.y)) + (c * (C.y));
    SUM->z = (a * (A.z)) + (b * (B.z)) + (c * (C.z));
}
void get_vector1(C_VECTOR *AB, COORDINATE A, COORDINATE B)
{
    AB->x = B.x - A.x;
    AB->y = B.y - A.y;
    AB->z = B.z - A.z;
}
C_VECTOR *get_vector(COORDINATE A, COORDINATE B)
{
    COORDINATE *BA = (COORDINATE*)malloc(sizeof(COORDINATE));
    BA->x = A.x - B.x;
    BA->y = A.y - B.y;
    BA->z = A.z - B.z;
    return (BA);
}
double *Get3DPlane(COORDINATE *p0, COORDINATE *p1, COORDINATE *p2, COORDINATE *p3)//=NULL)
{
    //Plane equation as Ax + By + Cz = D
    double *coeff = (double*)malloc(sizeof(double)*4);
    C_VECTOR *P01;
    C_VECTOR *P02;
    C_VECTOR Pcp;
    double dPcp;
    P01 = get_vector( *p1, *p0);//hm_sub_vector(*p0,*p1);
    P02 = get_vector( *p2, *p0);//hm_sub_vector(*p0,*p2);
    crossProduct(&Pcp, *P01, *P02);//hm_cross_product(P01,P02);
    coeff[0]=Pcp.x;
    coeff[1]=Pcp.y;
    coeff[2]=Pcp.z;
    coeff[3]=(Pcp.x * (p0->x)) + (Pcp.y * (p0->y)) + (Pcp.z * (p0->z));
    /*dPcp = sqrt( (Pcp.x * Pcp.x) + (Pcp.y * Pcp.y) + (Pcp.z * Pcp.z) );
    if(dPcp<FZ)
    {
      cout<<"dbg_Get3DPlane:1: 3 points (p0~2) are collinear"<<endl;
      return(coeff);
    }*/
    if( fabs(coeff[0]) < FZ )
        if( fabs(coeff[1]) < FZ)
            if(fabs(coeff[2]) < FZ)
            {
                cout<<"dbg_Get3DPlane:1: 3 points (p0~2) are collinear"<<endl;
                return(coeff);
            }
    if(p3!=NULL)
    {
        double d = (Pcp.x * (p3->x)) + (Pcp.y * (p3->y)) + (Pcp.z * (p3->z));
        if(fabs(d-coeff[3])>LZ)
        {
            cout<<"dbg_Get3DPlane:2: 4th of box (p3) point does not lie on the same plane"<<endl;
        }
    }
    return(coeff);
}

void divide_block_p(block_ ancestor_blk, long long int cut_x, long long int cut_y, long long int cut_z, vector<block_>& sub_blocks, vector<COORDINATE>& centers)
{
    block_ sub_blk;
    long long int x_min, y_min, z_min,nx,ny,nz;
    long long int x_max, y_max, z_max;
    x_min = ancestor_blk.MIN.x;
    y_min = ancestor_blk.MIN.y;
    z_min = ancestor_blk.MIN.z;
    x_max = ancestor_blk.MAX.x;
    y_max = ancestor_blk.MAX.y;
    z_max = ancestor_blk.MAX.z;
    //cout<<x_min<< " " <<y_min << " " <<z_min << " " << x_max << " " << y_max << " " << z_max << endl;
    long long int dif_x, dif_y, dif_z;
    dif_x = x_max - x_min;
    dif_y = y_max - y_min;
    dif_z = z_max - z_min;
    if(cut_x==0 || cut_y==0 || cut_z==0)
    {
        cout<<"ERROR: cut lenght error "<<cut_x<<" "<<cut_y<<" "<<cut_z<<endl;
        return ;
    }
    if(dif_x == 0 || dif_x < cut_x)
    {
        nx = 1;
    }
    else
    {
        nx = dif_x / cut_x;
        if(dif_x%cut_x)
            nx++;
    }
    if(dif_y == 0 || dif_y < cut_y)
    {
        ny = 1;
    }
    else
    {
         ny = dif_y / cut_y;
        if(dif_y%cut_y)
            ny++;
    }
    if(dif_z == 0 || dif_z < cut_z)
    {
        nz = 1;
    }
    else
    {
        nz = dif_z / cut_z;
        if(dif_z%cut_z)
            nz++;
    }
    long long int child = 0;
    //cout<<"dbg_divide_block: nx("<<nx<<") ny("<<ny<<") nz("<<nz<<")\n";
    for(int i = 0; i < nx; i++)
    {
        for(int j = 0; j < ny; j++)
        {
            for(int k = 0; k < nz; k++)
            {
                sub_blk.MIN.x = (i * cut_x) + x_min;
                sub_blk.MIN.y = (j * cut_y) + y_min;
                sub_blk.MIN.z = (k * cut_z) + z_min;
                if(i==nx-1)
                {
                    sub_blk.MAX.x = x_max;
                }
                else
                {
                    sub_blk.MAX.x = ((i + 1) *cut_x) + x_min;
                }
                if(j==ny-1)
                {
                    sub_blk.MAX.y = y_max;
                }
                else
                {
                    sub_blk.MAX.y = ((j + 1) * cut_y) + y_min;
                }
                if(k == nz-1)
                {
                    sub_blk.MAX.z = z_max;
                }
                else
                {
                    sub_blk.MAX.z = ((k + 1) * cut_z) + z_min;
                }
                sub_blocks.push_back(sub_blk);
                double center_x,center_y,center_z;
                center_x = (double)(sub_blk.MAX.x + sub_blk.MIN.x)/2.0;
                center_y = (double)(sub_blk.MAX.y + sub_blk.MIN.y)/2.0;
                center_z = (double)(sub_blk.MAX.z + sub_blk.MIN.z)/2.0;
                //cout<<"dbg_divide_block:\n";
                //cout<<sub_blk.MIN.x<<" "<<sub_blk.MIN.y<<" "<<sub_blk.MIN.z<<endl;
                //cout<<sub_blk.MAX.x<<" "<<sub_blk.MAX.y<<" "<<sub_blk.MAX.z<<endl;
                //cout<<"\t"<<i<<"_"<<j<<"_"<<k<<": "<<center_x<<" "<<center_y<<" "<<center_z<<endl;
                COORDINATE center_blk;
                center_blk.x = center_x; //floor (center_x + 0.5);
                center_blk.y = center_y; //floor (center_y + 0.5);
                center_blk.z = center_z; //floor (center_z + 0.5);
                centers.push_back(center_blk);
            }
        }
    }
}
