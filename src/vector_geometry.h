#define int64_t long long int
#define  C_VECTOR COORDINATE
#define FZ 1e-10
#define LZ 1e-6
typedef struct int_point
{
    long long int x,y,z;
    int_point()
    {
        x = 0;
        y = 0;
        z = 0;
    };
    int_point(int64_t X, int64_t Y, int64_t Z)
    {
      x = X;
      y = Y;
      z = Z;
    }
    void setxyz(int64_t X, int64_t Y, int64_t Z)
    {
      x = X;
      y = Y;
      z = Z;
    }
}point;

class COORDINATE
{
public:
    double x;
    double y;
    double z;
};

class block_
{
public:
    int ID; //optional
    point MIN, MAX;
    block_()
    {
        ID = 0;
        MIN.x=0;
        MIN.y=0;
        MIN.z=0;
        MAX.x=0;
        MAX.y=0;
        MAX.z=0;
    }
};

double dotProduct(C_VECTOR a, C_VECTOR b);
void crossProduct(C_VECTOR *c, C_VECTOR a, C_VECTOR b);
void vec_scalar_mul_add_2(C_VECTOR *SUM, double a, C_VECTOR A, double b, C_VECTOR B);
void vec_scalar_mul_add_3(C_VECTOR *SUM, double a, C_VECTOR A, double b, C_VECTOR B, double c, C_VECTOR C);
void get_vector1(C_VECTOR *AB, COORDINATE A, COORDINATE B);
void get_vector(C_VECTOR *BA, COORDINATE A, COORDINATE B);
double *Get3DPlane(COORDINATE *p0, COORDINATE *p1, COORDINATE *p2, COORDINATE *p3=NULL);

void divide_block_p(block_ ancestor_blk, long long int cut_x, long long int cut_y, long long int cut_z, vector<block_>& sub_blocks, vector<COORDINATE>& centers);

