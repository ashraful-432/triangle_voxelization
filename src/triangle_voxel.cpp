#include <bits/stdc++.h>
using namespace std;
#include "vector_geometry.h"

void copy_coordinate(COORDINATE *dst, COORDINATE *src)
{
    dst->x = src->x;
    dst->y = src->y;
    dst->z = src->z;
}
void copy_point(point *A, point B)
{
    A->x=B.x;
    A->y=B.y;
    A->z=B.z;
}
void copy_point_to_cordnt(point src, COORDINATE *dst)
{
    dst->x=(double)src.x;
    dst->y=(double)src.y;
    dst->z=(double)src.z;
}
void copy_cordnt_to_point(COORDINATE src, point *dst)
{
    dst->x = floor(src.x + 0.5);
    dst->y = floor(src.y + 0.5);
    dst->z = floor(src.z + 0.5);
}
void copy_cordnt_to_point_vec(vector<COORDINATE>& src, vector<point*>& dst)
{
    for (int i = 0; i < src.size(); i++)
    {
        point *pdst = new point[1];
        pdst->x = floor(src.at(i).x + 0.5);
        pdst->y = floor(src.at(i).y + 0.5);
        pdst->z = floor(src.at(i).z + 0.5);
        dst.push_back(pdst);
    }
}
void print_point_vec(const vector<point*>& points)
{
    for (int i = 0; i<points.size(); i++)
    {
        cout<<i<<": ("<<points.at(i)->x<<" "<<points.at(i)->y<<" "<<points.at(i)->z<<")\n";
    }
}

void print_tr_point(const vector<point*>& points, const vector<int>& edge)
{
    if(points.size() != edge.size())
    {
        cout<<"Error in print_tr_point\n";
        exit;
    }
    for (int i = 0; i<points.size(); i++)
    {
        cout<<i<<": ("<<points.at(i)->x<<" "<<points.at(i)->y<<" "<<points.at(i)->z<<") ";
        cout<<"["<<edge.at(i)<<"]\n";
    }
}

//---------------------------------------------------------------------------------------
double sq_dist_point_to_plane(double *coeff, COORDINATE *P)
{
    double sq_dist; //Square of distance
    sq_dist = (coeff[0] * P->x) + (coeff[1] * P->y) + (coeff[2] * P->z) + coeff[3];
    sq_dist = sq_dist * sq_dist;
    sq_dist = sq_dist / ((coeff[0]*coeff[0])+(coeff[1]*coeff[1])+(coeff[2]*coeff[2]));
    return sq_dist;
}
//---------------------------------------------------------------------------------------
bool inside_triangle(COORDINATE A, COORDINATE B, COORDINATE C, C_VECTOR n, COORDINATE Q)
{
    C_VECTOR AB, BC, CA, AQ, BQ, CQ;
    C_VECTOR AB_AQ, BC_BQ, CA_CQ;
    get_vector1(&AB, A, B);
    get_vector1(&BC, B, C);
    get_vector1(&CA, C, A);
    get_vector1(&AQ, A, Q);
    get_vector1(&BQ, B, Q);
    get_vector1(&CQ, C, Q);

    crossProduct(&AB_AQ, AB, AQ);
    crossProduct(&BC_BQ, BC, BQ);
    crossProduct(&CA_CQ, CA, CQ);
    double d1 = dotProduct(AB_AQ, n);
    double d2 = dotProduct(BC_BQ, n);
    double d3 = dotProduct(CA_CQ, n);
    if(d1>=0 && d2>=0 && d3>=0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
//---------------------------------------------------------------------------------------
//Triangle Minimum Bounding Box
void Tr_MINbb(const point& A, const point& B, const point& C, block_ *tr_minbb)
{
    long long int x_min, y_min, z_min;
    long long int x_max, y_max, z_max;
    x_min = min(A.x, B.x);
    x_min = min(x_min, C.x);
    y_min = min(A.y, B.y);
    y_min = min(y_min, C.y);
    z_min = min(A.z, B.z);
    z_min = min(z_min, C.z);

    x_max = max(A.x, B.x);
    x_max = max(x_max, C.x);
    y_max = max(A.y, B.y);
    y_max = max(y_max, C.y);
    z_max = max(A.z, B.z);
    z_max = max(z_max, C.z);

    tr_minbb->MIN.x = x_min;
    tr_minbb->MIN.y = y_min;
    tr_minbb->MIN.z = z_min;
    tr_minbb->MAX.x = x_max;
    tr_minbb->MAX.y = y_max;
    tr_minbb->MAX.z = z_max;
}
//---------------------------------------------------------------------------------------
int checker_2d (block_ *tr_minbb)
{
    if(tr_minbb->MIN.x == tr_minbb->MAX.x)
    {
        return 1;
    }
    else if(tr_minbb->MIN.y == tr_minbb->MAX.y)
    {
        return 2;
    }
    else if(tr_minbb->MIN.z == tr_minbb->MAX.z)
    {
        return 3;
    }
    else
    {
        return 0;
    }
}
bool tr_boundary_check(COORDINATE *Q, block_ *tr_minbb)
{
    int triangle_2d = checker_2d(tr_minbb);
    long long int x, y, z, diff_min_x,diff_max_x,diff_min_y,diff_max_y,diff_min_z, diff_max_z ;
    x = floor(Q->x + 0.5);
    y = floor(Q->y + 0.5);
    z = floor(Q->z + 0.5);

    diff_min_x = abs(x - tr_minbb->MIN.x);
    diff_min_y = abs(y - tr_minbb->MIN.y);
    diff_min_z = abs(z - tr_minbb->MIN.z);
    diff_max_x = abs(x - tr_minbb->MAX.x);
    diff_max_y = abs(y - tr_minbb->MAX.y);
    diff_max_z = abs(z - tr_minbb->MAX.z);

    if(triangle_2d == 1)
    {
        if(diff_min_y <=1 || diff_max_y <= 1 )
        {
            return true;
        }

        if(diff_min_z <= 1 || diff_max_z <= 1)
        {
            return true;
        }
    }
    else if(triangle_2d == 2)
    {
        if(diff_min_x <= 1 || diff_max_x <= 1)
        {
            return true;
        }
        if(diff_min_z <= 1 || diff_max_z <= 1)
        {
            return true;
        }
    }
    else if(triangle_2d == 3)
    {
        if(diff_min_x <= 1 || diff_max_x <= 1)
        {
            return true;
        }
        if(diff_min_y <=1 || diff_max_y <= 1 )
        {
            return true;
        }

    }
    else
    {
        if(diff_min_x <= 1 || diff_max_x <= 1 || diff_min_y <=1 || diff_max_y <= 1|| diff_min_z <= 1 || diff_max_z <= 1)
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------
void find_v_blk_cntr(COORDINATE A, COORDINATE B, COORDINATE C, C_VECTOR n, double *coeff,
                     double L_half_sq, vector<COORDINATE>& list_centers, vector<COORDINATE>& valid_centers,
                     vector<int>& flags, block_ *tr_box)
{
    // Vectors of triangle
    COORDINATE Q;
    int edge_flags = 0;
    C_VECTOR AB, BC, CA, AQ, BQ, CQ;
    C_VECTOR AB_AQ, BC_BQ, CA_CQ;
    get_vector1(&AB, A, B);
    get_vector1(&BC, B, C);
    get_vector1(&CA, C, A);
    // calculate 1/(A^2 + B^2 + C^2)
    double denom = 1.0 / ((coeff[0] * coeff[0]) + (coeff[1] * coeff[1]) + (coeff[2] * coeff[2]));
    for (int i = 0; i < list_centers.size(); i++)
    {
        copy_coordinate(&Q, &list_centers.at(i)); //get the sub-block center point as Q
        // calculte the square distance from plane to Q
        double sq_dist; //Square of distance
        sq_dist = (coeff[0] * Q.x) + (coeff[1] * Q.y) + (coeff[2] * Q.z) + coeff[3];
        sq_dist = sq_dist * sq_dist;
        sq_dist = sq_dist * denom;// / ((coeff[0] * coeff[0]) + (coeff[1] * coeff[1]) + (coeff[2] * coeff[2]));
        if (fabs(sq_dist) < L_half_sq)
        {
            // Q is in proximity to triangle plane
            get_vector1(&AQ, A, Q);
            get_vector1(&BQ, B, Q);
            get_vector1(&CQ, C, Q);
            crossProduct(&AB_AQ, AB, AQ);
            crossProduct(&BC_BQ, BC, BQ);
            crossProduct(&CA_CQ, CA, CQ);
            double d1 = dotProduct(AB_AQ, n);
            double d2 = dotProduct(BC_BQ, n);
            double d3 = dotProduct(CA_CQ, n);
            if (d1 >= 0 && d2 >= 0 && d3 >= 0)
            {
                //found valid center point
                //cout << "Q is inside the triangle\n";
                valid_centers.push_back(Q);
                if (edge_flags == 0) // i-1 was out of triangle
                {
                    edge_flags = 1;  // i is inside triangle at edge
                }
                else if (edge_flags == 1) //i-1 was at edge
                {
                    if(tr_boundary_check(&Q, tr_box))
                    {
                        edge_flags = 1;  // i is inside triangle and at boundary edge
                    }
                    else
                    {
                        edge_flags = 2;  // i is inside triangle
                    }
                }
                flags.push_back(edge_flags);
            }// if (d1 >= 0 && d2 >= 0 && d3 >= 0)
            else
            {
                //cout << "Q is outside the triangle\n";
                if (edge_flags == 2 || edge_flags == 1) //i-1 was inside triangle whereas the i is outside triangle
                {
                    vector<int>::reverse_iterator rit = flags.rbegin();
                    //flags[i - 1] = 1; //which means i-1 was at edge
                    *rit = 1;
                }
                edge_flags = 0;
            }
        } //if (fabs(sq_dist) < L_half_sq)
        else
        {
            //cout << "Q does not lies on the same plane of triangle\n";
            if (edge_flags == 2 || edge_flags == 1) //i-1 was inside triangle whereas the i is outside triangle
            {
                vector<int>::reverse_iterator rit = flags.rbegin();
                //flags[i - 1] = 1; //which means i-1 was at edge
                *rit = 1;
            }
            edge_flags = 0;
        }
    } //for (int i = 0; i < list_centers.size(); i++)
}
//---------------------------------------------------------------------------------------
bool div_triangle_v(point A, point B, point C, int64_t res_x, int64_t res_y, int64_t res_z,
                    point& bbMIN, point& bbMAX, vector<point*>& point_list, vector<int>& flags)
{
    COORDINATE A_, B_, C_;
    block_ *tr_box = new block_[1];
    vector<block_> tr_sub_blocks;
    vector<COORDINATE> sub_centers;
    vector<COORDINATE> valid_sub_centers;
    int64_t L; //6-neighborhood connectivity distance, L should be half of the max. resolution in +ve and -ve direction

    Tr_MINbb(A, B, C, tr_box);
   // cout<<tr_box->MIN.x << " " << tr_box->MIN.y << " " << tr_box->MIN.z << endl;
    //cout<<tr_box->MAX.x << " " << tr_box->MAX.y << " " << tr_box->MAX.z << endl;
    L = max(res_x, res_y);
    L = max(L, res_z);
    //cout << "L = " << L << endl;
    double L_ = (double)L;
    L_ = L_ * L_;
    L_ = L_ / 4.0; //Since we are comparing as square(L/2)
    // Now find the plane equation
    copy_point_to_cordnt(A, &A_);
    copy_point_to_cordnt(B, &B_);
    copy_point_to_cordnt(C, &C_);
    double *tr_pl_equ = Get3DPlane(&A_, &B_, &C_); //returns Ax+By+Cz=D
    tr_pl_equ[3] = -1.0 * tr_pl_equ[3];
    C_VECTOR tr_pl_n; //Plane norm vector
    tr_pl_n.x = tr_pl_equ[0];
    tr_pl_n.y = tr_pl_equ[1];
    tr_pl_n.z = tr_pl_equ[2];
    //cout << "tr_pl_equ: " << tr_pl_equ[0] << "x + " << tr_pl_equ[1] << "y + " << tr_pl_equ[2] << "z + " << tr_pl_equ[3] << " = 0\n";
    long long int cut_x = res_x;// / 2;
    long long int cut_y = res_y;// / 2;
    long long int cut_z = res_z;// / 2;
    divide_block_p(*tr_box, cut_x, cut_y, cut_z, tr_sub_blocks, sub_centers);
    //print_coord_vec(sub_centers);

    find_v_blk_cntr(A_, B_, C_, tr_pl_n, tr_pl_equ, L_, sub_centers,
                    valid_sub_centers, flags, tr_box);
    copy_cordnt_to_point_vec(valid_sub_centers, point_list);

    bbMIN.x = tr_box->MIN.x;
    bbMIN.y = tr_box->MIN.y;
    bbMIN.z = tr_box->MIN.z;
    bbMAX.x = tr_box->MAX.x;
    bbMAX.y = tr_box->MAX.y;
    bbMAX.z = tr_box->MAX.z;
    if(valid_sub_centers.size()==0)
    {
        cout<<"dbg_div_triangle_v: (1) size 0\n";
        return false;
    }
    else if(valid_sub_centers.size() != point_list.size())
    {
        cout<<"dbg_div_triangle_v: (2) size unequal\n";
    }
    return true;
}
//---------------------------------------------------------------------------------------
void test_init_point(const char *name, point *po, int i=-1)
{
    int64_t t;
    if(i<0)
    {
        cout << " " << name << " (x,,,) = ";
        cin >> t;
        po->x = t;
        cout << " " << name << " (,y,) = ";
        cin >> t;
        po->y = t;
        cout << " " << name << " (,,z) = ";
        cin >> t;
        po->z = t;
    }
    else
    {
        cout <<" "<<name<<"["<<i<<"] (x,,,) = ";
        cin >> t;
        po->x = t;
        cout <<" "<<name<<"["<<i<<"](,y,) = ";
        cin >> t;
        po->y = t;
        cout <<" "<<name<<"["<<i<<"](,,z) = ";
        cin >> t;
        po->z = t;
    }
}
//---------------------------------------------------------------------------------------
void test_print_point(const char *name, point *po, int i=-1)
{
    if(i<0)
    {
        cout << " " << name << " ( ";
        cout<<po->x<<" ";
        cout<<po->y<<" ";
        cout<<po->z<<" )\n";
    }
    else
    {
        cout <<" "<<name<<"["<<i<<"] = ( ";
        cout<<po->x<<" ";
        cout<<po->y<<" ";
        cout<<po->z<<" )\n";
    }
}
void test_print_block(block_ *blk)
{
    test_print_point("MIN", &blk->MIN);
    test_print_point("MAX", &blk->MAX);
}
//---------------------------------------------------------------------------------------
bool pointset2vtk(const char *fname, int vox_size, vector<point*>pointset);
int main()
{
    vector<point*>all_point;
    point A, B, C, Q;
    COORDINATE A_, B_, C_, Q_;
    int64_t voxel_size;
    int64_t res_x, res_y, res_z;
    cout << "insert voxel size (integer and multiples of 2) ";/*in X, Y, Z direction-\n x_";
    cin >> res_x;
    cout << "y_ ";
    cin >> res_y;
    cout << "z_ ";
    cin >> res_z;*/
    cin >> voxel_size;
    res_x = voxel_size;
    res_y = voxel_size;
    res_z = voxel_size;
    cout << "insert the triangle vertices in counter clockwise\n";
    test_init_point("A", &A);
    test_init_point("B", &B);
    test_init_point("C", &C);
    cout << "Finding the points in given triangles proximity\n";
    vector<point*>resulted_points;
    vector<int> edges;
    point min_of_bb, max_of_bb;
    div_triangle_v(A, B, C, res_x, res_y, res_z, min_of_bb, max_of_bb, resulted_points, edges);
    print_point_vec(resulted_points);
    pointset2vtk("tr.vtk", voxel_size, resulted_points);
}
