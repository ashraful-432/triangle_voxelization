#include <bits/stdc++.h>
using namespace std;
#include "vector_geometry.h"

bool pointset2vtk(const char *fname, int vox_size, vector<point*>pointset)
{
  ofstream vtkfile;
  vtkfile.open(fname);
  vtkfile<<"# vtk DataFile Version 2.0\nVTK Output\nASCII\n\n";
  vtkfile<<"DATASET UNSTRUCTURED_GRID\nPOINTS "<<8*pointset.size()<<" int\n";
  int v = vox_size/2;
  for (int i = 0; i<pointset.size(); i++)
  {

    vtkfile<<pointset[i]->x-v<<" "<<pointset[i]->y-v<<" "<<pointset[i]->z-v<<"\n"; //0
    vtkfile<<pointset[i]->x+v<<" "<<pointset[i]->y-v<<" "<<pointset[i]->z-v<<"\n"; //1
    vtkfile<<pointset[i]->x-v<<" "<<pointset[i]->y+v<<" "<<pointset[i]->z-v<<"\n"; //2
    vtkfile<<pointset[i]->x+v<<" "<<pointset[i]->y+v<<" "<<pointset[i]->z-v<<"\n"; //3
    vtkfile<<pointset[i]->x-v<<" "<<pointset[i]->y-v<<" "<<pointset[i]->z+v<<"\n"; //4
    vtkfile<<pointset[i]->x+v<<" "<<pointset[i]->y-v<<" "<<pointset[i]->z+v<<"\n"; //5
    vtkfile<<pointset[i]->x-v<<" "<<pointset[i]->y+v<<" "<<pointset[i]->z+v<<"\n"; //6
    vtkfile<<pointset[i]->x+v<<" "<<pointset[i]->y+v<<" "<<pointset[i]->z+v<<"\n\n"; //7
  }
  vtkfile<<"CELLS "<<pointset.size()<<" "<<pointset.size()*9<<"\n";
  for (int i = 0; i<pointset.size(); i++)
  {
    int j = i*8;
    vtkfile<<"8 "<<j+0<<" "<<j+1<<" "<<j+2<<" "<<j+3<<" "<<j+4<<" "<<j+5<<" "<<j+6<<" "<<j+7<<"\n";
  }
  vtkfile<<"\nCELL_TYPES "<<pointset.size()<<"\n";
  for (int k = 0; k<pointset.size(); k++)
  {
    vtkfile<<"11\n";
  }
  vtkfile.close();
  return true;
}
