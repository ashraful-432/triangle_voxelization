# triangle voxelization

This source code implements Triangle voxelization. The method is implemented based on the following Paper-

"An Accurate Method for Voxelizing Polygon Meshes"
http://web.eecs.utk.edu/~huangj/papers/polygon.pdf

Inputs are Voxel dimension and Triangle coordinates. Output is VTK file(tr.vtk).

compilation command in linux-

g++ -std=c++11 triangle_voxel.cpp vector_geometry.cpp vector_geometry.h pointset2vtk.cpp -o tr_voxel

!(/Capture.PNG)